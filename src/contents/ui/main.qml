import QtQuick 2.1
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

Kirigami.ApplicationWindow {
    id: root

    title: "Weather"

    width: 360
    height: 600

  
    globalDrawer: Kirigami.GlobalDrawer { 
        title: "Hello App"
        titleIcon: "applications-graphics"
        actions: [
            Kirigami.Action {
                text: "View"
                iconName: "view-list-icons"
                Kirigami.Action {
                    text: "action 1"
                }
                Kirigami.Action {
                    text: "action 2"
                }
                Kirigami.Action {
                    text: "action 3"
                }
            },
            Kirigami.Action {
                text: "action 3"
            },
            Kirigami.Action {
                text: "action 4"
            }
        ]
    }    
    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    pageStack.initialPage: mainPageComponent

    Component {
        id: mainPageComponent

        Kirigami.Page {
            id: pane
            title: "Weather"

            Rectangle{

                // id: pane
                color: "white"
                anchors.fill: parent
                ColumnLayout {
                    spacing: 0
                    anchors.fill: parent
                
                    Image {
                        id: logo
                        Layout.alignment: Qt.AlignCenter
                        Layout.preferredWidth: pane.availableHeight / 6
                        Layout.preferredHeight: pane.availableWidth / 6
                        fillMode: Image.PreserveAspectFit
                        function getIcon(val){
                            if(val == 1)
                                return "sunny.png"
                            else if(val == 2)
                                return "cloudy.png"
                            else 
                                return "rainy.png"
                        }
                        source: getIcon(2)
                    }
                    Label{
                        text:  qsTr("Hello Kirigami")
                        color: "black"
                        Layout.alignment: Qt.AlignCenter
                    }
                }
            }
        }
    }
}
